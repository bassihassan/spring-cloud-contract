package me.bassihassan.demo.validation;


import me.bassihassan.demo.exception.BusinessException;
import me.bassihassan.demo.exception.ExceptionPayload;
import me.bassihassan.demo.exception.ExceptionPayloadFactory;

import java.util.Collection;
import java.util.regex.Pattern;

public interface Assert {

    static void assertNotNull(Object value) {
        if (value == null) {
            throw new BusinessException(ExceptionPayloadFactory.INVALID_PAYLOAD.get());
        }
    }


    static void assertNotEmpty(Collection<?> value) {
        if (value == null || value.isEmpty()) {
            throw new BusinessException(ExceptionPayloadFactory.INVALID_PAYLOAD.get());
        }
    }

    static void assertNotEmpty(Collection<?> value, ExceptionPayload payload) {
        if (value == null || value.isEmpty()) {
            throw new BusinessException(payload);
        }
    }

    static void assertMinLength(String value, int minLength) {
        if (value == null || value.length() < minLength) {
            throw new BusinessException(ExceptionPayloadFactory.INVALID_PAYLOAD.get());
        }
    }

    static void assertMaxLength(String value, int maxLength) {
        if (value == null || value.length() > maxLength) {
            throw new BusinessException(ExceptionPayloadFactory.INVALID_PAYLOAD.get());
        }
    }

    static void assertTrue(Boolean value) {
        if (!Boolean.TRUE.equals(value)) {
            throw new BusinessException(ExceptionPayloadFactory.INVALID_PAYLOAD.get());
        }
    }

    static void assertRegex(String regex, String value, ExceptionPayload exceptionPayload) {
        if (!Pattern.compile(regex).matcher(value).matches())
            throw new BusinessException(exceptionPayload);
    }
}
