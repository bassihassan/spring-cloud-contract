package me.bassihassan.demo.dto;

import me.bassihassan.demo.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class UserMapper {


    @Mapping(source = "age",target = "age")
    public abstract UserDTO map(User user);
}
