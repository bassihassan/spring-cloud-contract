package me.bassihassan.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDTO {
    public String name;
    public Integer age;
}
