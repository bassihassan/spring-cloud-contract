package me.bassihassan.demo;

import lombok.val;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {

        SpringApplication.run(DemoApplication.class, args);
    }


 /*
    @Entity
    @Table(name = "users")
    @AllArgsConstructor
    @Getter
    @Setter
    @NoArgsConstructor
    @Builder
    public static class User {

        @Id
        @Builder.Default
        private String id = UUID.randomUUID().toString();
        private String name;
    }

  */

}
