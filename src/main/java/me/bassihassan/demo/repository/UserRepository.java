package me.bassihassan.demo.repository;

import me.bassihassan.demo.api.criteria.UserCriteria;
import me.bassihassan.demo.domain.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {


    default Page<User> findByCriteria(UserCriteria userCriteria, Pageable pageable) {

        Example<User> userExample = null;

        return findAll(userExample, pageable);
    }
}
