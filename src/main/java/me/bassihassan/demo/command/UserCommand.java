package me.bassihassan.demo.command;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import me.bassihassan.demo.core.Validatable;
import me.bassihassan.demo.validation.Assert;

@AllArgsConstructor
@Getter
@ToString
public class UserCommand implements Validatable {
    public final String name;
    public final Integer age;

    @Override
    public void validate() {
        Assert.assertNotNull(name);
        Assert.assertNotNull(age);
        Assert.assertMinLength(name, 2);
    }
}
