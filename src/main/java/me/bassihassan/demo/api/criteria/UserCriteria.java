package me.bassihassan.demo.api.criteria;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserCriteria {
    public final String name;
    public final Integer age;
}
