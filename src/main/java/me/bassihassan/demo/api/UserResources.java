package me.bassihassan.demo.api;

import lombok.RequiredArgsConstructor;
import me.bassihassan.demo.api.criteria.UserCriteria;
import me.bassihassan.demo.command.UserCommand;
import me.bassihassan.demo.domain.User;
import me.bassihassan.demo.dto.UserDTO;
import me.bassihassan.demo.dto.UserMapper;
import me.bassihassan.demo.service.user.UserService;
import me.bassihassan.demo.service.user.UserViewService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class UserResources {

    private final UserService userService;
    private final UserViewService userViewService;
    private final UserMapper userMapper;

    @PostMapping("/users")
    public ResponseEntity<UserDTO> create(@RequestBody final UserCommand userCommand) {
        userCommand.validate();
        User user = userService.createUser(userCommand);
        return ResponseEntity.ok(userMapper.map(user));

    }

    @GetMapping("/users")
    public ResponseEntity<Page<UserDTO>> get(@ModelAttribute final UserCriteria userCriteria, final Pageable pageable) {
        return ResponseEntity.ok(userViewService.findAll(userCriteria, pageable));

    }
}
