package me.bassihassan.demo.domain;

import lombok.*;
import me.bassihassan.demo.command.UserCommand;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
public class User extends BaseEntity {

    private String name;
    private Integer age;


    public static User createUser(UserCommand command) {
        // validation rules
        return new User(command.getName(), command.getAge());
    }

    public void disableUser() {
        this.setActive(false);
    }
}
