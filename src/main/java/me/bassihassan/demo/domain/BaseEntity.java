package me.bassihassan.demo.domain;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@MappedSuperclass
@Getter
@EntityListeners(AuditingEntityListener.class)
@Setter
public class BaseEntity {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    protected String id;
/*
    @Column(name = "CREATED_BY", updatable = false, nullable = false)
    @Size(min = 1, max = 255)
    @CreatedBy
    protected String createdBy;

    @Column(name = "CREATED_DATE", updatable = false)
    @CreatedDate
    protected Date createdDate;

    @Column(name = "UPDATED_BY")
    @Size(min = 1, max = 255)
    @LastModifiedBy
    protected String lastModifiedBy;

    @Column(name = "UPDATED_DATE")
    @LastModifiedDate
    protected Date lastModifiedDate;
*/
    @Version
    protected Integer version;

    @Column(name = "ACTIVE")
    protected Boolean active = Boolean.TRUE;
}
