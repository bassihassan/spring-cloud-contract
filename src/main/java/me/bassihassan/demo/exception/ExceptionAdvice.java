package me.bassihassan.demo.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.UUID;


@RestControllerAdvice
@Slf4j
@RestController
public class ExceptionAdvice {

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity<ExceptionPayload> handle(final BusinessException throwable) {
        log.error("BusinessException occurred {} ", throwable.getExceptionPayload(), throwable);
        final ExceptionPayload exceptionPayload = throwable.getExceptionPayload();
        return ResponseEntity.status(exceptionPayload.getStatusCode()).body(exceptionPayload);
    }


    @ExceptionHandler({Throwable.class})
    public ResponseEntity<ExceptionPayload> handle(final Throwable throwable) {
        final String guid = UUID.randomUUID().toString();
        final ExceptionPayload exceptionPayload = ExceptionPayloadFactory.UNKNOWN_ERROR.get().toBuilder().reference(guid).build();
        log.error("error occurred with reference {} ", guid, throwable);
        return ResponseEntity.status(exceptionPayload.getStatusCode()).body(exceptionPayload);
    }
}
