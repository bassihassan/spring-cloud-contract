package me.bassihassan.demo.exception;


import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.Date;

public enum ExceptionPayloadFactory {
    UNKNOWN_ERROR(1, "UNKNOWN.ERROR", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    INVALID_PAYLOAD(2, "INVALID.PAYLOAD", HttpStatus.BAD_REQUEST.value()),
    ;
    private ExceptionPayload exceptionPayload;

    ExceptionPayloadFactory(final Integer code, final String message, final Integer statusCode) {
        this.exceptionPayload = ExceptionPayload.builder()
                .code(code)
                .message(message)
                .statusCode(statusCode)
                .timestamp(Date.from(Instant.now()))
                .build();
    }

    public ExceptionPayload get() {
        return exceptionPayload;
    }
}
