package me.bassihassan.demo.exception;

import javax.validation.constraints.NotNull;

public class BusinessException extends RuntimeException {
    private final ExceptionPayload exceptionPayload;

    public BusinessException(@NotNull ExceptionPayload exceptionPayload) {
        super(exceptionPayload.getMessage());
        this.exceptionPayload = exceptionPayload;
    }

    public ExceptionPayload getExceptionPayload() {
        return exceptionPayload;
    }
}
