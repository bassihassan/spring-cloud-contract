package me.bassihassan.demo.service.user;

import me.bassihassan.demo.api.criteria.UserCriteria;
import me.bassihassan.demo.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserViewService {

    Page<UserDTO> findAll(UserCriteria userCriteria, Pageable pageable);
}
