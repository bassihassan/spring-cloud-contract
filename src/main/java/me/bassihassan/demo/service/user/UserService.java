package me.bassihassan.demo.service.user;

import me.bassihassan.demo.command.UserCommand;
import me.bassihassan.demo.domain.User;

public interface UserService {

    User createUser(UserCommand userCommand);

}
