package me.bassihassan.demo.service.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.bassihassan.demo.command.UserCommand;
import me.bassihassan.demo.domain.User;
import me.bassihassan.demo.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public User createUser(UserCommand userCommand) {
        log.info("begin user creation with payload {}", userCommand);
        User user = User.createUser(userCommand);
        log.info("user created successfully with payload {} and id {}", userCommand, user.getId());
        return userRepository.save(user);
    }

}
