package me.bassihassan.demo.service.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.bassihassan.demo.api.criteria.UserCriteria;
import me.bassihassan.demo.dto.UserDTO;
import me.bassihassan.demo.dto.UserMapper;
import me.bassihassan.demo.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserViewServiceImp implements UserViewService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;


    @Override
    public Page<UserDTO> findAll(UserCriteria userCriteria, Pageable pageable) {
        return userRepository.findByCriteria(userCriteria, pageable).map(userMapper::map);
    }
}
