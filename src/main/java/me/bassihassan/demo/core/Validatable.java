package me.bassihassan.demo.core;

public interface Validatable {

    void validate();
}
