#!/bin/bash

STUB_JAR_NAME='stub-runner.jar';
STUB_RUNNER_URL='https://repo1.maven.org/maven2/org/springframework/cloud/spring-cloud-contract-stub-runner-boot/3.1.0/spring-cloud-contract-stub-runner-boot-3.1.0.jar';

if [[ ! -f $STUB_JAR_NAME ]]; then
  curl -Lo $STUB_JAR_NAME $STUB_RUNNER_URL
fi

java -jar stub-runner.jar  --server.port=9091 --stubrunner.ids=me.bassihassan:toto:+:9093 \
 --stubrunner.stubsMode=LOCAL --stubrunner.workOffline=true
